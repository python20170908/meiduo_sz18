import os
from collections import OrderedDict
from goods.models import GoodsCategory, GoodsChannel
from .models import ContentCategory, Content
from django.shortcuts import render
from django.conf import settings
from utils.category import get_category_list


def generate_index_html():
    # 1.查询数据
    # 1.1查询所有分类
    categories = get_category_list()

    # 1.2查询所有广告
    contents = {}
    content_categories = ContentCategory.objects.all()
    for content_category in content_categories:
        contents[content_category.key] = content_category.content_set.filter(status=True).order_by('sequence')

    # 2.生成html标签，保存到html文件中
    # 2.1生成html标签
    response = render(None, 'index.html', {'categories': categories, 'contents': contents})
    html_str = response.content.decode()
    # 2.2保存html文件
    html_path = os.path.join(settings.GENERATE_STATIC_HTML, 'index.html')
    with open(html_path, 'w', encoding='utf-8') as f:
        f.write(html_str)

    print("OK")

from django.db import models
from utils.models import BaseModel


# Create your models here.
class QQUser(BaseModel):
    openid = models.CharField(max_length=64, db_index=True)
    user = models.ForeignKey('users.User')

    class Meta:
        db_table = 'tb_oauth_qq'

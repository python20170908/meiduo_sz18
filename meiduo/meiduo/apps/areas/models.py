from django.db import models


# 深圳市
# parent===>广东省
# area_set--->subs====>宝山，南山,....

class Area(models.Model):
    # 行政区划名称
    name = models.CharField(max_length=20)
    # 上级行政区划，省是没有上级的，所以可以为空
    parent = models.ForeignKey('self', related_name='subs', null=True, blank=True)

    class Meta:
        db_table = 'tb_areas'

    def __str__(self):
        return self.name

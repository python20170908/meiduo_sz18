from django.conf.urls import url
from . import views
from rest_framework.routers import DefaultRouter

urlpatterns = [

]

router = DefaultRouter()
# 自动生成单数、复数的路由规则
router.register('areas', views.AreaViewSet, base_name='areas')
urlpatterns += router.urls

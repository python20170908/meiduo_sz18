# from django.contrib import admin
# from . import models
# from celery_tasks.html.tasks import generate_static_sku_detail_html
#
#
# class SKUAdmin(admin.ModelAdmin):
#     list_display = ['id', 'name']
#
#     def save_model(self, request, obj, form, change):
#         # 当对象进行添加、修改时，会执行此方法
#         super().save_model(request, obj, form, change)
#         generate_static_sku_detail_html.delay(obj.id)
#
#         # def delete_model(self, request, obj):
#         #     #当删除对象时会执行此方法
#         #     obj.is_delete=True
#         #     obj.save()
#         #
#         #     pass
#
#
# admin.site.register(models.GoodsCategory)
# admin.site.register(models.GoodsChannel)
# admin.site.register(models.Goods)
# admin.site.register(models.Brand)
# admin.site.register(models.GoodsSpecification)
# admin.site.register(models.SpecificationOption)
# admin.site.register(models.SKU, SKUAdmin)
# admin.site.register(models.SKUSpecification)
# admin.site.register(models.SKUImage)

from rest_framework import generics
from .models import SKU
from . import serializers
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from utils.pagination import SKUPageNumberPagination
from drf_haystack.viewsets import HaystackViewSet
from .serializers import SKUIndexSerializer


class SKUListView(generics.ListAPIView):
    # queryset =SKU.objects.filter(category_id=)
    # 分页
    pagination_class = SKUPageNumberPagination
    # 排序
    filter_backends = [OrderingFilter]
    ordering_fields = ('id', 'price', 'sales')

    def get_queryset(self):
        # 路径中的位置参数self.args[]
        # 路径中的关键字参数self.kwargs[]
        return SKU.objects.filter(category_id=self.kwargs['category_id'])

    serializer_class = serializers.SKUSerializer


class SKUSearchViewSet(HaystackViewSet):
    """
    SKU搜索
    """
    index_models = [SKU]

    serializer_class = SKUIndexSerializer

    pagination_class = SKUPageNumberPagination

from django.contrib.auth.backends import ModelBackend
from .models import User
import re


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user_id': user.id,
        'username': user.username
    }


class MyAuthenticationBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        # 根据username查询对象
        if re.match(r'^1[3-9]\d{9}$', username):
            # 当前为手机号
            user = User.objects.get(mobile=username)
        else:
            # 当前为用户名
            user = User.objects.get(username=username)

        # 判断密码是否正确
        if user.check_password(password):
            return user
        else:
            return None
            # 返回查询对的用户对象

from django.db import models
from django.contrib.auth.models import AbstractUser
from utils import tjws
from . import constants
from utils.models import BaseModel


# from django.contrib.auth import authenticate
# from django.contrib.auth.backends import ModelBackend
class User(AbstractUser):
    # 自定义扩展用户类，在用户名、密码、邮箱等属性的基础上，扩展属性
    mobile = models.CharField(max_length=11, unique=True)
    email_active = models.BooleanField(default=False)
    # 默认收货地址
    default_address = models.OneToOneField('users.Address', related_name='users', null=True, blank=True)

    class Meta:
        db_table = 'tb_users'

    def verify_email_url(self):
        # url?token=****
        token = tjws.dumps({'user_id': self.id}, constants.VERIFY_EMAIL_EXPIRES)
        return 'http://www.meiduo.site:8080/success_verify_email.html?token=' + token


class Address(BaseModel):
    user = models.ForeignKey(User, related_name='addresses')
    title = models.CharField(max_length=10)
    receiver = models.CharField(max_length=10)
    province = models.ForeignKey('areas.Area', related_name='province_addr')
    city = models.ForeignKey('areas.Area', related_name='city_addr')
    district = models.ForeignKey('areas.Area', related_name='district_addr')
    place = models.CharField(max_length=50)
    mobile = models.CharField(max_length=11)
    tel = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=50, null=True, blank=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'tb_address'

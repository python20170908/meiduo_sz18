from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from .models import User, Address
from . import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework.decorators import action
from goods.serializers import SKUSerializer
from django_redis import get_redis_connection
from goods.models import SKU
from rest_framework_jwt.views import ObtainJSONWebToken
from carts.utils import merge_cookie_to_redis


class UsernameCountView(APIView):
    def get(self, request, username):
        # 判断用户名是否存在：根据用户名在数据库中做统计
        count = User.objects.filter(username=username).count()
        return Response({
            'username': username,
            'count': count
        })


class MobileCountView(APIView):
    def get(self, request, mobile):
        # 判断手机号是否存在：统计
        count = User.objects.filter(mobile=mobile).count()
        return Response({
            'mobile': mobile,
            'count': count
        })


class UserCreateView(generics.CreateAPIView):
    # queryset =
    serializer_class = serializers.UserCreateSerializer


class UserDetailView(generics.RetrieveAPIView):
    # queryset =
    serializer_class = serializers.UserDetailSerializer
    # 必须登录
    permission_classes = [IsAuthenticated]

    # 默认获取一个对象的方法为：根据主键查询
    # 当前逻辑为：哪个用户登录，则显示这个用户的信息
    def get_object(self):
        return self.request.user


class EmailView(generics.UpdateAPIView):
    # 修改当前登录用户的邮箱
    serializer_class = serializers.EmailSerializer
    permission_classes = [IsAuthenticated]

    # queryset =
    def get_object(self):
        return self.request.user


class EmailVerifyView(APIView):
    def get(self, request):
        # 接收数据
        serializer = serializers.EmailVerifySerializer(data=request.query_params)
        # 验证
        if serializer.is_valid():
            # 保存修改
            serializer.save()
            return Response({"message": "OK"})
        # 错误提示
        return Response({'message': serializer.errors})


class AddressViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]

    # retrieve====>无用
    # create===>直接使用
    # update===>直接使用

    # list
    def list(self, request, *args, **kwargs):
        addresses = self.get_queryset()
        serializer = self.get_serializer(addresses, many=True)

        return Response({
            'user_id': request.user.id,
            'default_address_id': request.user.default_address_id,
            'limit': 5,
            'addresses': serializer.data  # [{},{},...]
        })

    # destroy：默认实现是物理删除，当前期望实现逻辑删除
    def destroy(self, request, *args, **kwargs):
        address = self.get_object()
        address.is_delete = True
        address.save()
        return Response(status=204)

    # queryset = Address.objects.all()
    def get_queryset(self):
        # 当前登录用户的未删除的收货地址
        return self.request.user.addresses.filter(is_delete=False)

    serializer_class = serializers.AddressSerializer

    # 修改标题===>***/pk/title/  ****/title/
    @action(methods=['put'], detail=True)
    def title(self, request, pk):
        # 接收请求报文中的标题
        title = request.data.get('title')
        # 修改对象的属性
        address = self.get_object()
        address.title = title
        address.save()
        # 响应
        return Response({'title': title})

    # 设置成默认===>****/pk/status/
    @action(methods=['put'], detail=True)
    def status(self, request, pk):
        # 获取当前登录的用户
        user = self.request.user
        # 修改这个用户的默认收货地址
        user.default_address_id = pk
        user.save()
        # 响应
        return Response({'message': 'OK'})


class BrowseHistoryView(generics.ListCreateAPIView):
    # queryset =
    permission_classes = [IsAuthenticated]

    # serializer_class = serializers.BrowseHistorySerializer
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SKUSerializer
        else:
            return serializers.BrowseHistorySerializer

    def get_queryset(self):
        # 从Redis中读取所有的商品编号
        redis_cli = get_redis_connection("history")
        key = 'history_%d' % self.request.user.id
        sku_ids = redis_cli.lrange(key, 0, -1)  # [b'1',b'2']
        # 在商品表中查询相应的数据
        skus = []
        for sku_id in sku_ids:
            skus.append(SKU.objects.get(pk=int(sku_id)))
        return skus


class UserLoginView(ObtainJSONWebToken):
    def post(self, request, *args, **kwargs):
        # 执行原有的验证逻辑
        response = super().post(request, *args, **kwargs)
        # 如果登录成功，则合并购物车
        if response.status_code == 200:
            # 获取用户的编号
            user_id = response.data.get('user_id')
            # 执行合并
            response = merge_cookie_to_redis(request, user_id, response)
        # 响应
        return response

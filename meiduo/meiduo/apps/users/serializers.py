from rest_framework import serializers
from django_redis import get_redis_connection
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.utils import jwt_payload_handler
import re
from .models import User, Address
from django.core.mail import send_mail
from django.conf import settings
from celery_tasks.email.tasks import send_verify_email
from utils import tjws
from . import constants
from goods.models import SKU
from django_redis import get_redis_connection


class UserCreateSerializer(serializers.Serializer):
    # 定义属性
    id = serializers.IntegerField(read_only=True)
    # 输出口令
    token = serializers.CharField(read_only=True)
    username = serializers.CharField(
        min_length=5,
        max_length=20,
        error_messages={
            'min_length': '用户名要求是5-20个字符',
            'max_length': '用户名要求是5-20个字符',
        }
    )
    password = serializers.CharField(
        min_length=8,
        max_length=20,
        error_messages={
            'min_length': '密码要求是8-20个字符',
            'max_length': '密码要求是8-20个字符',
        },
        write_only=True
    )
    password2 = serializers.CharField(
        min_length=8,
        max_length=20,
        error_messages={
            'min_length': '确认密码要求是8-20个字符',
            'max_length': '确认密码要求是8-20个字符',
        },
        # 属性只接收客户端的数据，不向客户端响应
        write_only=True
    )
    sms_code = serializers.IntegerField(write_only=True)
    mobile = serializers.CharField()
    allow = serializers.BooleanField(write_only=True)

    # 验证validate_**
    # 验证用户名是否存在
    def validate_username(self, value):
        count = User.objects.filter(username=value).count()
        if count > 0:
            raise serializers.ValidationError('用户名已经存在')
        return value

    # 验证手机号
    def validate_mobile(self, value):
        # 正则匹配
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')

        # 手机号是否重复
        count = User.objects.filter(mobile=value).count()
        if count > 0:
            raise serializers.ValidationError('手机号已经存在')
        return value

    # 验证是否同意协议
    def validate_allow(self, value):
        if not value:
            raise serializers.ValidationError('请先阅读并同意协议')
        return value

    def validate(self, attrs):
        # attrs表示请求的所有数据，类型为字典

        # 验证密码是否一致
        password = attrs.get('password')
        password2 = attrs.get('password2')
        if password != password2:
            raise serializers.ValidationError('两个密码不一致')

        # 验证短信码
        sms_code_request = attrs.get('sms_code')
        mobile = attrs.get('mobile')
        # 读取redis中的值
        redis_cli = get_redis_connection('sms_code')
        sms_code_redis = redis_cli.get('sms_code' + mobile)
        # 判断是否有验证码
        if not sms_code_redis:
            raise serializers.ValidationError('短信验证码已经过期')
        # 强制当前验证码过期
        redis_cli.delete('sms_code' + mobile)
        # 判断两个验证码是否相等
        if int(sms_code_redis) != int(sms_code_request):
            raise serializers.ValidationError('短信验证码错误')

        return attrs

    # 创建create
    def create(self, validated_data):
        # user=User.objects.create(**validated_data)
        user = User()
        user.username = validated_data.get('username')
        user.set_password(validated_data.get('password'))
        user.mobile = validated_data.get('mobile')
        user.save()

        # 当用户注册成功后，认为登录，则需要进行状态保持===>获取token
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token

        return user


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'mobile', 'email', 'email_active']


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['email']

    def update(self, instance, validated_data):
        email = validated_data.get('email')
        instance.email = email
        instance.save()

        # 发送激活邮件
        # send_mail('美多商城-邮箱激活','',settings.EMAIL_FROM,[email],html_message='<a>你好</a>')
        # 发邮件耗时，加入celery中
        url = instance.verify_email_url()
        send_verify_email.delay(email, url)

        return instance


class EmailVerifySerializer(serializers.Serializer):
    # 接收激活参数
    token = serializers.CharField(max_length=200)

    # 验证激活参数是否有效
    def validate_token(self, value):
        data_dict = tjws.loads(value, constants.VERIFY_EMAIL_EXPIRES)
        if data_dict is None:
            raise serializers.ValidationError('激活链接已经失效')
        # value =
        return data_dict.get('user_id')

    # 激活：修改用户的email_active属性
    def create(self, validated_data):
        user = User.objects.get(pk=validated_data.get('token'))
        user.email_active = True
        user.save()
        return user


class AddressSerializer(serializers.ModelSerializer):
    # 明确定义隐藏属性
    province_id = serializers.IntegerField()
    city_id = serializers.IntegerField()
    district_id = serializers.IntegerField()

    # 将关系属性以字符串输出
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Address
        exclude = ['is_delete', 'user', 'create_time', 'update_time']

    def create(self, validated_data):
        # 外键user并没有从客户端传递过来，而是使用当前登录的用户
        validated_data['user'] = self.context['request'].user

        return super().create(validated_data)


class BrowseHistorySerializer(serializers.Serializer):
    sku_id = serializers.IntegerField()

    def validate_sku_id(self, value):
        count = SKU.objects.filter(pk=value).count()
        if count <= 0:
            raise serializers.ValidationError('商品编号无效')
        return value

    def create(self, validated_data):
        sku_id = validated_data.get('sku_id')
        # 键
        key = 'history_%d' % self.context['request'].user.id
        # 连接redis
        redis_cli = get_redis_connection('history')
        # 1.删除
        redis_cli.lrem(key, 0, sku_id)
        # 2.左加
        redis_cli.lpush(key, sku_id)
        # 3.判断长度
        if redis_cli.llen(key) > constants.BROWSE_HISTORY_LIMIT:
            # 4.超过限制则右出
            redis_cli.rpop(key)
        return sku_id

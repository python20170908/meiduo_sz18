from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import CartAddSerializer, SKUSerializer, CartDeleteSerializer, CartSelectAllSerializer
from utils import myjson
from goods.models import SKU
from . import constants
from django_redis import get_redis_connection


class CartView(APIView):
    def perform_authentication(self, request):
        # 进行身份验证，但是未登录时，没有可以用于验证的信息，则会抛401错误
        # 不让APIView帮助我们做验证，重写此函数
        pass

    def get(self, request):
        try:
            user = request.user
        except:
            user = None

        # 判断用户是否登录
        if user is not None:
            # 登录后从Redis中读取数据
            redis_cli = get_redis_connection('cart')
            key = 'cart_%d' % request.user.id
            # hash
            sku_ids = redis_cli.hkeys(key)
            skus = SKU.objects.filter(pk__in=sku_ids)
            # set
            sku_ids_select = redis_cli.smembers('cart_selected_%d' % request.user.id)
            sku_ids_select = [int(sku_id) for sku_id in sku_ids_select]
            for sku in skus:
                sku.count = redis_cli.hget(key, sku.id)
                sku.selected = sku.id in sku_ids_select
        else:
            # 未登录从cookie中读取数据
            cart_str = request.COOKIES.get('cart')
            if cart_str is not None:
                cart_dict = myjson.loads(cart_str)
            else:
                cart_dict = {}
            # 读取所有的键，查询SKU对象
            skus = []
            for sku_id in cart_dict.keys():
                # 查询商品对象
                sku = SKU.objects.get(pk=sku_id)
                # 增加数量属性
                sku.count = cart_dict[sku_id]['count']
                # 增加选中状态属性
                sku.selected = cart_dict[sku_id]['selected']
                skus.append(sku)
        # 序列化
        serializer = SKUSerializer(skus, many=True)
        return Response(serializer.data)

    def post(self, request):
        # 接收参数
        data = request.data

        # 验证
        serializer = CartAddSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']

        # 创建
        # 判断用户是否登录
        try:
            # 当禁用APIView的验证后，调用此代码时会验证
            user = request.user
        except:
            user = None

        response = Response(serializer.validated_data)

        if user is not None and user.is_authenticated:
            # 已登录则存redis
            redis_cli = get_redis_connection('cart')
            # hash
            redis_cli.hincrby('cart_%d' % request.user.id, sku_id, count)
            # set
            redis_cli.sadd('cart_selected_%d' % request.user.id, sku_id)
        else:
            # 未登录则存cookie
            # 读取cookie中的购物车信息
            cart_str = request.COOKIES.get('cart')
            # 如果没有购物车则新建
            if cart_str is None:
                # 如果有则在现有数据上添加
                cart_dict = {}
            else:
                # 如果有则获取原字典
                cart_dict = myjson.loads(cart_str)
            # 获取当前商品在购物车中的数量
            if sku_id in cart_dict:
                count_cart = cart_dict[sku_id]['count']
            else:
                count_cart = 0

            # 在购物车中加入商品
            cart_dict[sku_id] = {
                'count': count + count_cart,
                'selected': selected
            }
            # 将字典转字符串
            cart_str = myjson.dumps(cart_dict)
            response.set_cookie('cart', cart_str, max_age=constants.CART_COOKIE_EXPIRES)

        return response

    def put(self, request):
        try:
            user = request.user
        except:
            user = None

        # 接收数据并验证
        serializer = CartAddSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']
        count = serializer.validated_data['count']
        selected = serializer.validated_data['selected']

        response = Response(serializer.validated_data)

        if user is not None:
            # 如果用户登录则操作redis
            redis_cli = get_redis_connection('cart')
            key1 = 'cart_%d' % request.user.id
            key2 = 'cart_selected_%d' % request.user.id
            # hash
            redis_cli.hset(key1, sku_id, count)
            # set
            if selected:
                redis_cli.sadd(key2, sku_id)
            else:
                redis_cli.srem(key2, sku_id)
        else:
            # 如果用户未登录则操作cookie
            # 从现有购物车中读取数据
            cart_str = request.COOKIES.get('cart')
            if cart_str is None:
                raise Exception('当前没有购物车数据')
            cart_dict = myjson.loads(cart_str)
            # 找到此商品的购物车数据
            sku = cart_dict[sku_id]
            # 修改
            sku['count'] = count
            sku['selected'] = selected
            # 输出cookie
            cart_str = myjson.dumps(cart_dict)
            response.set_cookie('cart', cart_str, max_age=constants.CART_COOKIE_EXPIRES)
        return response

    def delete(self, request):
        try:
            user = request.user
        except:
            user = None

        serializer = CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sku_id = serializer.validated_data['sku_id']

        response = Response(status=204)

        if user is None:
            # 操作cookie
            # 读
            cart_str = request.COOKIES.get('cart')
            if cart_str is None:
                raise Exception('暂无购物车数据')
            cart_dict = myjson.loads(cart_str)
            # 删
            if sku_id in cart_dict:
                del cart_dict[sku_id]
            # 写
            cart_str = myjson.dumps(cart_dict)
            response.set_cookie('cart', cart_str, max_age=constants.CART_COOKIE_EXPIRES)
        else:
            # 操作Redis
            redis_cli = get_redis_connection('cart')
            key1 = 'cart_%d' % request.user.id
            key2 = 'cart_selected_%d' % request.user.id
            # hash
            redis_cli.hdel(key1, sku_id)
            # set
            redis_cli.srem(key2, sku_id)
        return response


class CartSelectAllView(APIView):
    def perform_authentication(self, request):
        pass

    def put(self, request):
        try:
            user = request.user
        except:
            user = None

        serializer = CartSelectAllSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        selected = serializer.validated_data['selected']

        response = Response({'message': 'ok'})

        if user is None:
            # 未登录
            # 读cookie
            cart_str = request.COOKIES.get('cart')
            if cart_str is None:
                raise Exception('无购物车数据')
            cart_dict = myjson.loads(cart_str)
            # 遍历修改
            for data in cart_dict.values():
                data['selected'] = selected
            # 写cookie
            cart_str = myjson.dumps(cart_dict)
            response.set_cookie('cart', cart_str, max_age=constants.CART_COOKIE_EXPIRES)
        else:
            # 已登录
            redis_cli = get_redis_connection('cart')
            key1 = 'cart_%d' % request.user.id
            key2 = 'cart_selected_%d' % request.user.id
            # 从hash中获取所有商品编号
            sku_ids = redis_cli.hkeys(key1)
            if selected:
                redis_cli.sadd(key2, *sku_ids)
            else:
                redis_cli.srem(key2, *sku_ids)

        return response

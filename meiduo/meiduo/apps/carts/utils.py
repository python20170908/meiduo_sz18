from django_redis import get_redis_connection
from utils import myjson


def merge_cookie_to_redis(request, user_id, response):
    '''
    在登录时，将cookie中的购物车信息，存入redis中，合并时以cookie中的数据为准
    :param request: 请求对象，用于读取cookie中的购物车数据
    :param user_id: 用户编号，用于redis中的键
    :param response: 响应对象，用于删除cookie中的购物车数据
    :return: response响应对象
    '''
    # 1.读取cookie中购物车数据
    # 1.1读取cookie中购物车信息，返回字符串
    cart_str = request.COOKIES.get('cart')
    #如果cookie中没有购物车信息，直接返回
    if cart_str is None:
        return response
    # 1.2将字符串转换成字典，方便操作
    cart_dict = myjson.loads(cart_str)
    '''
    {
        商品编号：{
            'count':数量,
            'selected':选中状态
        }
    }
    '''

    # 2.存入redis中
    # 2.1连接redis
    redis_cli = get_redis_connection('cart')
    # 2.1.1构造redis中的键
    key_user = 'cart_%d' % user_id
    key_selected = 'cart_selected_%d' % user_id
    # 2.2使用管道对象，执行多条命令
    redis_pipeline = redis_cli.pipeline()
    # 2.3遍历字典，向Redis中添加数据
    for key_sku, value in cart_dict.items():
        # 2.3.1向hash中添加商品、数量
        redis_pipeline.hset(key_user, key_sku, value['count'])
        # 2.3.2向set中维护选中状态
        if value['selected']:
            redis_pipeline.sadd(key_selected, key_sku)
        else:
            redis_pipeline.srem(key_selected, key_sku)
    # 2.4执行命令
    redis_pipeline.execute()

    # 3.删除cookie中购物车数据
    response.set_cookie('cart', '', max_age=0)

    return response

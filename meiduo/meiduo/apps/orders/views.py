from rest_framework.views import APIView
from rest_framework.response import Response
from django_redis import get_redis_connection
from goods.models import SKU
from rest_framework.permissions import IsAuthenticated
from .serializers import OrderSettleSerializer, OrderCreateSerializer
from rest_framework.generics import CreateAPIView


class OrderSettleView(APIView):
    # 用户登录后，根据用户的编号，在redis中获取购物车数据cart_用户编号
    permission_classes = [IsAuthenticated]

    def get(self, request):
        # 获取用户编号，构造redis中购物车的键
        key_cart = 'cart_%d' % request.user.id
        key_select = 'cart_selected_%d' % request.user.id
        # 从redis中获取当前选中的商品
        redis_cli = get_redis_connection('cart')
        cart_dict = redis_cli.hgetall(key_cart)  # [{编号：数量},...]
        # {b'10': b'1', b'1': b'2', b'16': b'3'}
        # 将byte===>int
        cart_dict2 = {}
        for key, value in cart_dict.items():
            cart_dict2[int(key)] = int(value)
        cart_selected = redis_cli.smembers(key_select)
        # 根据商品编号查询商品对象
        skus = SKU.objects.filter(pk__in=cart_selected)
        # 遍历，增加数量属性
        for sku in skus:
            sku.count = cart_dict2[sku.id]
        # 序列化输出
        result_dict = {
            'freight': 10,
            'skus': skus
        }
        serializer = OrderSettleSerializer(result_dict)
        return Response(serializer.data)


class OrderCreateView(CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = OrderCreateSerializer

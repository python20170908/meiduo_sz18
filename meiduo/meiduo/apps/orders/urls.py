from django.conf.urls import url
from . import views

urlpatterns = [
    url('^orders/settlement/$', views.OrderSettleView.as_view()),
    url('^orders/$', views.OrderCreateView.as_view()),
]

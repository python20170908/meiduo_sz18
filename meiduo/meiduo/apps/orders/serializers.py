from rest_framework import serializers
from goods.models import SKU
from users.models import Address
from .models import OrderInfo, OrderGoods
from datetime import datetime
from django_redis import get_redis_connection
from django.db import transaction
import time


class SKUCartSerializer(serializers.ModelSerializer):
    count = serializers.IntegerField()

    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'count']


class OrderSettleSerializer(serializers.Serializer):
    freight = serializers.DecimalField(max_digits=5, decimal_places=2)
    skus = SKUCartSerializer(many=True)


class OrderCreateSerializer(serializers.Serializer):
    # 定义属性，验证方法，create和update方法
    # ModelSerializer定义好的内容是：属性,create和update方法
    address = serializers.IntegerField(write_only=True)
    pay_method = serializers.IntegerField(write_only=True)
    order_id = serializers.CharField(read_only=True)

    # 验证
    def validate_address(self, value):
        count = Address.objects.filter(pk=value).count()
        if count <= 0:
            raise serializers.ValidationError('收货地址无效')
        return value

    def validate_pay_method(self, value):
        if value not in [1, 2]:
            raise serializers.ValidationError('付款方式无效')
        return value

    # 创建
    def create(self, validated_data):
        with transaction.atomic():
            # 开启事务，以后运行的sql语句都在事务中
            save_id = transaction.savepoint()
            # 获取用户编号
            user_id = self.context['request'].user.id
            # 获取请求数据
            address = validated_data.get('address')
            pay_method = validated_data.get('pay_method')
            # 1.创建订单对象
            total_count = 0
            total_amount = 0
            order_id = datetime.now().strftime('%Y%m%d%H%M%S') + '%09d' % user_id
            order = OrderInfo.objects.create(
                order_id=order_id,
                user_id=user_id,
                address_id=address,
                total_count=0,
                total_amount=0,
                freight=10,
                pay_method=pay_method
            )
            # 2.查询购物车中选中的商品及数量
            redis_cli = get_redis_connection('cart')
            cart_dict = redis_cli.hgetall('cart_%d' % user_id)
            cart_selected = redis_cli.smembers('cart_selected_%d' % user_id)
            cart_dict2 = {}
            cart_selected2 = []
            for key, value in cart_dict.items():
                cart_dict2[int(key)] = int(value)
            cart_selected2 = [int(sku_id) for sku_id in cart_selected]
            # 3.查询选中的商品对象
            skus = SKU.objects.filter(pk__in=cart_selected2)
            # 4.遍历
            for sku in skus:
                # 获取商品购买量
                cart_count = cart_dict2[sku.id]
                # 4.1判断库存
                if sku.stock < cart_count:
                    # 回滚事务
                    transaction.savepoint_rollback(save_id)
                    # 抛出异常
                    raise serializers.ValidationError('商品库存不足')

                # 演示并发效果
                # time.sleep(5)

                # 4.2修改商品的库存、销量
                # sku.stock -= cart_count
                # sku.sales += cart_count
                # sku.save()

                # 4.2升级为乐观锁，解决并发问题
                stock = sku.stock - cart_count
                sales = sku.sales + cart_count
                # 如果修改数据成功则返回1，否则返回0
                result = SKU.objects.filter(pk=sku.id, stock=sku.stock).update(stock=stock, sales=sales)
                if result == 0:
                    transaction.savepoint_rollback(save_id)
                    raise serializers.ValidationError('当前购买人数太多，请重新购买')

                # 4.3创建订单商品对象
                OrderGoods.objects.create(
                    order_id=order_id,
                    sku_id=sku.id,
                    count=cart_count,
                    price=sku.price
                )
                # 4.4计算总购买量、总金额
                total_count += cart_count
                total_amount += cart_count * sku.price
            # 4.5修改订单的总数量、总金额
            order.total_count = total_count
            order.total_amount = total_amount
            order.save()
            # 提交事务
            transaction.savepoint_commit(save_id)
            # 5.删除购物车中选中的商品hash、set
            redis_cli.hdel('cart_%d' % user_id, *cart_selected2)
            redis_cli.srem('cart_selected_%d' % user_id, *cart_selected2)
            # 返回新建的订单对象
            return order

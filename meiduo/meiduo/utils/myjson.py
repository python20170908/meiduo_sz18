import pickle
import base64


def dumps(my_dict):
    # b'\x**\x**...'
    json_bytes = pickle.dumps(my_dict)
    # b'a-zA-Z0-9'
    json_64 = base64.b64encode(json_bytes)
    # bytes--->字符串
    return json_64.decode()


def loads(my_str):
    # str====>bytes
    json_64 = my_str.encode()
    # b'a-zA-Z0-9'====>b'\x**\x**...'
    json_bytes = base64.b64decode(json_64)
    # bytes===>dict
    return pickle.loads(json_bytes)

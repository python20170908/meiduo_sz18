from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible
from fdfs_client.client import Fdfs_client
from django.conf import settings


@deconstructible
class FdfsStorage(Storage):
    def __init__(self, client_conf=None, storage_url=None):
        self.client_conf = client_conf or settings.FDFS_CLIENT_CONF
        self.storage_url = storage_url or settings.FDFS_STORAGE_URL

    def open(self, name, mode='rb'):
        # 直接从storage中读取文件，不需要通过这个类读取
        pass

    def save(self, name, content, max_length=None):
        # 文件对象content===》从请求报文中获取
        # 创建对象
        client = Fdfs_client(self.client_conf)
        # 上传
        ret = client.upload_by_buffer(content.read())
        # 判断是否上传成功
        if ret['Status'] != 'Upload successed.':
            raise Exception('文件上传失败')
        # 返回文件名
        return ret['Remote file_id']

    def url(self, name):
        return self.storage_url + name

    def exists(self, name):
        return False

from fdfs_client.client import Fdfs_client

if __name__ == '__main__':
    # 创建对象，指定配置文件
    client = Fdfs_client('client.conf')
    # 调用方法，上传
    ret = client.upload_by_file('/home/python/Desktop/pic/avatar/1.jpg')
    '''
    {
    'Uploaded size': '12.00KB',
    'Status': 'Upload successed.',
    'Group name': 'group1',
    'Storage IP': '192.168.247.128',
    'Local file name': '/home/python/Desktop/pic/avatar/1.jpg',
    'Remote file_id': 'group1/M00/00/02/wKj3gFvaTxaAR1AuAAAwL_xHUtE187.jpg'
    }
    '''
    print(ret)

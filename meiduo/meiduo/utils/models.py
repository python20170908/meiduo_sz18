from django.db import models

#封装两个属性，其它模型类继承
class BaseModel(models.Model):
    # 创建时间，自动设置为当前时间,添加时设置
    create_time = models.DateTimeField(auto_now_add=True)
    # 修改时间，每次修改时都会设置为当前时间
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        # 设置成抽象，表示此模型类不与表对应
        abstract = True
